#!/bin/sh -e
# Description: run ttq with some example queries and verify it does not crash
# Options: native
# https://postmarketos.org/pmb-ci
set -x

./ttq.py -h
./ttq.py
./ttq.py librem
./ttq.py pinephone=
./ttq.py pinephone= -m -n -o
./ttq.py pinephone= -f
./ttq.py soc:sdm845
./ttq.py ui:
./ttq.py ui:sxmo
