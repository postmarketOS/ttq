# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import argparse
import sys

import ttq.wiki

ARGS = None
ARGS_QUERY_SOCS = {}
ARGS_QUERY_DEVICES = []
ARGS_QUERY_UI = ""
ARGS_USER_TYPE = "gitlab"
BULLET_POINT = "*"

def parse_args():
    global ARGS
    global BULLET_POINT

    ttq = sys.argv[0]
    examples = ("examples:\n"
                "  list users by device:\n"
                f"  $ {ttq}  # all\n"
                f"  $ {ttq} librem  # matching *librem*\n"
                f"  $ {ttq} pinephone=  # matching *pinephone (not the pro)\n"
                "\n"
                "  list users by SoC/device:\n"
                f"  $ {ttq} soc:sdm845  # matching *sdm845*\n"
                "\n"
                "  list users by UI:\n"
                f"  $ {ttq} ui:  # all\n"
                f"  $ {ttq} ui:sxmo  # matching *sxmo*\n")


    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                                     description=examples)
    parser.add_argument("-o", "--offline", action="store_true",
                        help="never treat cached files as outdated")
    parser.add_argument("-f", "--force-update", action="store_true",
                        help="force update of cached files")
    parser.add_argument("-m", "--matrix", action="store_true",
                        help="show matrix users instead of gitlab users")
    parser.add_argument("-n", "--notes", action="store_true",
                        help="print notes (if any)")
    parser.add_argument("-c", "--checkboxes", action="store_true",
                        help="Use checkboxes instead of bullet points")
    parser.add_argument("QUERY", nargs="?",
                        help="what to search for")

    ARGS = parser.parse_args()
    if ARGS.checkboxes:
        BULLET_POINT = "* [ ]"


def parse_args_query_soc():
    global ARGS_QUERY_DEVICES
    global ARGS_QUERY_SOCS

    search_str = ARGS.QUERY.split("soc:", 1)[1]

    ttq.wiki.parse_socs()
    for soc in ttq.wiki.SOCS:
        if search_str in soc.lower():
            devices = ttq.wiki.parse_soc_devices(soc)
            print(f"Found {len(devices)} device(s) with {soc}", file=sys.stderr)
            ARGS_QUERY_SOCS[soc] = devices

    # Put devices in ARGS_QUERY_DEVICES so ttq.match.by_device() can use it
    for soc, devices in ARGS_QUERY_SOCS.items():
        for device in devices:
            if device not in ARGS_QUERY_DEVICES:
                ARGS_QUERY_DEVICES += [device]

    if ARGS_QUERY_SOCS:
        return

    print(f"Available SoCs ({len(ttq.wiki.SOCS)}):")
    for soc in ttq.wiki.SOCS:
        print(f"* {soc}")
    print()
    print(f"ERROR: did not match any SoC with '{search_str}'")
    sys.exit(1)


def parse_args_query():
    global ARGS_QUERY_DEVICES
    global ARGS_QUERY_UI

    query = ARGS.QUERY
    if not query:
        return
    if query.startswith("ui:"):
        ARGS_QUERY_UI = query.split("ui:", 1)[1]
    elif query.startswith("soc:"):
        parse_args_query_soc()
    else:
        ARGS_QUERY_DEVICES = [query]


def parse_args_user_type():
    global ARGS_USER_TYPE

    if ARGS.matrix:
        ARGS_USER_TYPE = "matrix"

