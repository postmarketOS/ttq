# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import ttq.args
import ttq.wiki

DEVICES_MATCHED = {}
UIS_MATCHED = {}
USERS_MATCHED = []
SOCS_MATCHED = {}
NOTES_MATCHED = {}


def by_device():
    global DEVICES_MATCHED
    global SOCS_MATCHED
    global USERS_MATCHED

    for user in ttq.wiki.TESTING_TEAM:
        for device in user["devices"]:
            if ttq.args.ARGS_QUERY_DEVICES:
                skip = True
                for query_device in ttq.args.ARGS_QUERY_DEVICES:
                    if query_device in f"={device}=":
                        skip = False
                if skip:
                    continue

            if user not in USERS_MATCHED:
                USERS_MATCHED += [user]

            if device not in DEVICES_MATCHED:
                DEVICES_MATCHED[device] = []
            DEVICES_MATCHED[device] += [user]

            for soc, devices in ttq.args.ARGS_QUERY_SOCS.items():
                if f"={device}=" not in devices:
                    continue
                if soc not in SOCS_MATCHED:
                    SOCS_MATCHED[soc] = []
                if device not in SOCS_MATCHED[soc]:
                    SOCS_MATCHED[soc] += [device]


def by_ui():
    global UIS_MATCHED
    global USERS_MATCHED

    for user in ttq.wiki.TESTING_TEAM:
        for ui in user["uis"]:
            # Don't count comments in the UI table as separate UIs
            if not ui.startswith("postmarketos-ui-"):
                continue

            if ttq.args.ARGS_QUERY_UI:
                if ttq.args.ARGS_QUERY_UI not in ui:
                    continue

            if user not in USERS_MATCHED:
                USERS_MATCHED += [user]

            if ui not in UIS_MATCHED:
                UIS_MATCHED[ui] = []
            UIS_MATCHED[ui] += [user]


def notes():
    global NOTES_MATCHED

    for user in USERS_MATCHED:
        notes = user["notes"]
        username = user["gitlab"]
        if not notes or notes.lower() == "n/a":
            continue
        NOTES_MATCHED[username] = notes
